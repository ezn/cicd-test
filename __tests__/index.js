const { expect } = require('chai');
const cal = require('../src/calculator');


describe('Calcolator test', () => {
  it ('Add', () => {
    expect(cal.add(1, 2)).equal(3)
    expect(cal.add(1, 2)).not.equal(4)
  })
  it ('Divide', () => {
    expect(cal.sub(5, 2)).equal(3)
    expect(cal.sub(1, 2)).not.equal(4)
  })
  it ('Multiply', () => {
    expect(cal.mul(5, 2)).equal(10)
    expect(cal.mul(1, 2)).not.equal(4)
  })
  it ('Division', () => {
    expect(cal.div(50, 2)).equal(25)
    expect(cal.div(1, 2)).not.equal(4)
  })
})