module.exports = {
  add(a, b) {
    if (typeof a !== "number") throw new Error('a is not a valid number')
    if (typeof b !== "number") throw new Error('b is not a valid number')
    return a + b
  },
  sub(a, b) {
    if (typeof a !== "number") throw new Error('a is not a valid number')
    if (typeof b !== "number") throw new Error('b is not a valid number')
    return a - b
  },
  mul(a, b) {
    if (typeof a !== "number") throw new Error('a is not a valid number')
    if (typeof b !== "number") throw new Error('b is not a valid number')
    return a * b
  },
  div(a, b) {
    if (typeof a !== "number") throw new Error('a is not a valid number')
    if (typeof b !== "number") throw new Error('b is not a valid number')
    return a / b
  },
  rem(a, b) {
    if (typeof a !== "number") throw new Error('a is not a valid number')
    if (typeof b !== "number") throw new Error('b is not a valid number')
    return a % b
  },
}